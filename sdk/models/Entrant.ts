/* tslint:disable */
import {
  Person,
  Pool
} from '../index';

declare var Object: any;
export interface EntrantInterface {
  "hashes": number;
  "personId": number;
  "poolId": number;
  "id"?: number;
  "poolHistoryId"?: number;
  person?: Person;
  pool?: Pool;
}

export class Entrant implements EntrantInterface {
  "hashes": number;
  "personId": number;
  "poolId": number;
  "id": number;
  "poolHistoryId": number;
  person: Person;
  pool: Pool;
  constructor(data?: EntrantInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Entrant`.
   */
  public static getModelName() {
    return "Entrant";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Entrant for dynamic purposes.
  **/
  public static factory(data: EntrantInterface): Entrant{
    return new Entrant(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Entrant',
      plural: 'Entrants',
      path: 'Entrants',
      idName: 'id',
      properties: {
        "hashes": {
          name: 'hashes',
          type: 'number',
          default: 0
        },
        "personId": {
          name: 'personId',
          type: 'number'
        },
        "poolId": {
          name: 'poolId',
          type: 'number'
        },
        "id": {
          name: 'id',
          type: 'number'
        },
        "poolHistoryId": {
          name: 'poolHistoryId',
          type: 'number'
        },
      },
      relations: {
        person: {
          name: 'person',
          type: 'Person',
          model: 'Person',
          relationType: 'belongsTo',
                  keyFrom: 'personId',
          keyTo: 'id'
        },
        pool: {
          name: 'pool',
          type: 'Pool',
          model: 'Pool',
          relationType: 'belongsTo',
                  keyFrom: 'poolId',
          keyTo: 'id'
        },
      }
    }
  }
}
